//! Module for the execution of subcommands.
//!
//! Getting the subcommand and its options from the user via the command line is handled in the
//! `crate::cli` module.

pub mod clean;
pub mod exec;
pub mod show;
