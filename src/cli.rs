//! Module to support converting the chosen subcommand and its associated options from command line
//! args to an enum.
//!
//! Execution of the subcommands is provided in the `crate::subcommands` module.

use std::env;
use std::path::PathBuf;

use anyhow::{anyhow, bail};
use casper_types::{BlockHash, Digest, Key};
use clap::{crate_description, crate_name, crate_version, Command};
use directories::ProjectDirs;

mod clean;
mod exec;
mod show;

/// An option which was either provided by the user on the command line, or a default value to use
/// if no corresponding cached option is available.
#[derive(Debug)]
pub enum UserProvidedOrDefault<T> {
    User(T),
    Default(T),
}

impl<T> UserProvidedOrDefault<T> {
    pub fn value(self) -> T {
        match self {
            UserProvidedOrDefault::User(value) | UserProvidedOrDefault::Default(value) => value,
        }
    }
}

/// Identifier for a snapshot of global state.
#[derive(Debug)]
pub enum SnapshotId {
    Latest,
    StateHash(Digest),
    BlockHeight(u64),
    BlockHash(BlockHash),
}

enum DisplayOrder {
    Exec,
    Show,
    Clean,
}

#[derive(Debug)]
pub enum Show {
    Key(Key),
    AllState,
    CachedConfig,
}

/// The subcommand and associated options passed via the command line.
#[derive(Debug)]
pub enum Cli {
    Exec {
        storage_dir: UserProvidedOrDefault<PathBuf>,
        chain_name: UserProvidedOrDefault<String>,
        node_address: UserProvidedOrDefault<String>,
        snapshot_id: UserProvidedOrDefault<SnapshotId>,
        transaction_path: PathBuf,
    },
    Show(Show),
    Clean,
}

impl Cli {
    pub fn build() -> anyhow::Result<Self> {
        let arg_matches = build().get_matches();
        let (subcommand_name, matches) = arg_matches.subcommand().ok_or_else(|| {
            let _ = build().print_long_help();
            anyhow!("failed to get subcommand")
        })?;

        match subcommand_name {
            exec::SUBCOMMAND_NAME => exec::get(matches),
            show::SUBCOMMAND_NAME => Ok(show::get(matches)),
            clean::SUBCOMMAND_NAME => Ok(Self::Clean),
            _ => bail!("{} is not a valid subcommand", subcommand_name),
        }
    }
}

fn build() -> Command {
    Command::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .subcommand(exec::build(DisplayOrder::Exec as usize))
        .subcommand(show::build(DisplayOrder::Show as usize))
        .subcommand(clean::build(DisplayOrder::Clean as usize))
}

fn default_storage_dir() -> PathBuf {
    if let Some(project_dir) = ProjectDirs::from("", "Casper Labs", crate_name!()) {
        return project_dir.data_dir().to_path_buf();
    }
    env::temp_dir().join(crate_name!())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        build().debug_assert()
    }
}
