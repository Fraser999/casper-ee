use std::fs;

use anyhow::{anyhow, Context};
use log::info;

use crate::CachedConfig;

pub fn run() -> anyhow::Result<()> {
    let cached_config = CachedConfig::try_read()?.ok_or_else(|| {
        anyhow!(
            "expected a cached config to exist at {} from a previous 'exec' run",
            CachedConfig::path().display()
        )
    })?;
    let dir = &cached_config.storage_dir;

    if !dir.exists() {
        info!("storage dir at {} doesn't exist", dir.display());
        return Ok(());
    }

    fs::remove_dir_all(dir)
        .with_context(|| format!("failed removing storage dir at {}", dir.display()))?;
    info!("removed storage dir at {}", dir.display());

    Ok(())
}
