use std::path::PathBuf;

use anyhow::{anyhow, Context};
use casper_client::rpcs::common::BlockIdentifier;
use casper_client::{JsonRpcId, Verbosity};
use casper_execution_engine::engine_state::{
    DeployItem, EngineConfig, EngineState, ExecuteRequest, ExecutionResult,
};
use casper_types::{
    account::AccountHash, execution::ExecutionResultV2, DeployConfig, Digest, Key, ProtocolVersion,
    PublicKey, Timestamp,
};
use log::{debug, info, trace};
use tokio::runtime::Handle;

use crate::{CachedConfig, SnapshotId, State, Storage, UserProvidedOrDefault};

pub fn run(
    storage_dir: UserProvidedOrDefault<PathBuf>,
    chain_name: UserProvidedOrDefault<String>,
    node_address: UserProvidedOrDefault<String>,
    snapshot_id: UserProvidedOrDefault<SnapshotId>,
    transaction_path: PathBuf,
    runtime: Handle,
) -> anyhow::Result<()> {
    let cached_config = match CachedConfig::try_read()? {
        Some(mut config) => {
            // Overwrite cached values with any provided by the user on this run.
            if let UserProvidedOrDefault::User(storage_dir) = storage_dir {
                config.storage_dir = storage_dir;
            }
            if let UserProvidedOrDefault::User(chain_name) = chain_name {
                config.chain_name = chain_name;
            }
            if let UserProvidedOrDefault::User(node_address) = node_address {
                config.node_address = node_address;
            }
            if let UserProvidedOrDefault::User(snapshot_id) = snapshot_id {
                let addr = config.node_address.clone();
                config.state_hash = get_state_root_hash(addr, snapshot_id, runtime)?;
            }

            config
        }
        None => {
            // If there's no cached values, just use whatever we got from CLI; either user-input or
            // defaults.
            let node_address = node_address.value();
            let state_hash =
                get_state_root_hash(node_address.clone(), snapshot_id.value(), runtime)?;
            CachedConfig {
                storage_dir: storage_dir.value(),
                chain_name: chain_name.value(),
                node_address,
                state_hash,
            }
        }
    };

    // Save the updated options.
    cached_config.write()?;

    let state_hash = cached_config.state_hash;

    // Try to read in the Transaction.
    let transaction = casper_client::read_deploy_file(&transaction_path).with_context(|| {
        format!(
            "failed to read Transaction at {}",
            transaction_path.display()
        )
    })?;

    // Check it's config compliant - this is checked by the deploy/transaction acceptor on the node.
    transaction
        .is_config_compliant(
            &cached_config.chain_name,
            &DeployConfig::default(), // TODO - get from chainspec
            100,                      // TODO - get from chainspec
            transaction.timestamp(),
        )
        .context("transaction is not config compliant")?;

    let account_key = Key::from(AccountHash::from(transaction.header().account()));

    // Construct the EE.
    let storage = Storage::new(
        &cached_config.storage_dir,
        &cached_config.chain_name,
        &state_hash,
        true,
    )?;
    let state = State::new(
        state_hash,
        storage.clone(),
        cached_config.node_address.clone(),
    );
    let engine_config = EngineConfig::default(); // TODO - get from chainspec
    let engine_state = EngineState::new(state.clone(), engine_config);

    // Execute the Transaction.
    let deploy_item = DeployItem::from(transaction);
    let execute_request = ExecuteRequest::new(
        state_hash,
        Timestamp::now().millis(),
        vec![deploy_item],
        ProtocolVersion::V1_0_0, // TODO - get from chainspec
        PublicKey::System,       // TODO - does this have issues?
    );
    let results = engine_state
        .run_execute(execute_request)
        .context("failed to execute the Transaction")?;
    assert_eq!(results.len(), 1, "should only be one execution result");
    let result = results.front().unwrap();
    trace!(
        "execution result: {}",
        serde_json::to_string_pretty(&ExecutionResultV2::from(result.clone())).unwrap()
    );
    match result {
        ExecutionResult::Failure { cost, error, .. } => {
            info!("execution failed with cost: {}, error: {}", cost, error)
        }
        ExecutionResult::Success { cost, .. } => info!("execution succeeded with cost: {}", cost),
    }

    // Save the changes to global state.
    let _ = engine_state
        .apply_effects(state_hash, result.effects().clone())
        .context("failed to save the changes to global state");
    storage.persist()?;

    if let Some(account) = storage
        .get(&account_key)
        .and_then(|stored_value| stored_value.as_account().cloned())
    {
        info!(
            "account after execution:\n{}",
            serde_json::to_string_pretty(&account).unwrap()
        );
    }

    Ok(())
}

fn rpc_id() -> JsonRpcId {
    JsonRpcId::Number(0)
}

/// If the user provided a snapshot ID of a block or "latest", get the state hash from the node.  If
/// they provided a state hash, just return that.
fn get_state_root_hash(
    node_address: String,
    snapshot_id: SnapshotId,
    runtime: Handle,
) -> anyhow::Result<Digest> {
    let maybe_block_id = match snapshot_id {
        SnapshotId::Latest => None,
        SnapshotId::StateHash(state_hash) => return Ok(state_hash),
        SnapshotId::BlockHeight(height) => Some(BlockIdentifier::Height(height)),
        SnapshotId::BlockHash(block_hash) => Some(BlockIdentifier::Hash(block_hash)),
    };

    let join_handle = std::thread::spawn(move || {
        runtime.block_on(async {
            casper_client::get_state_root_hash(
                rpc_id(),
                &node_address,
                Verbosity::Low,
                maybe_block_id,
            )
            .await
        })
    });

    let response = join_handle
        .join()
        .expect("thread should join")
        .context("failed to get state hash from node")?;
    debug!("got state root hash response: {:?}", response);
    response
        .result
        .state_root_hash
        .ok_or_else(|| anyhow!("requested state hash not found on node"))
}
