use anyhow::anyhow;
use casper_types::Key;

use crate::{CachedConfig, Storage};

pub fn value(key: Key) -> anyhow::Result<()> {
    let cached_config = get_cached_config()?;

    let storage = Storage::new(
        &cached_config.storage_dir,
        &cached_config.chain_name,
        &cached_config.state_hash,
        false,
    )?;

    match storage.get(&key) {
        Some(value) => println!("{}", serde_json::to_string_pretty(&value).unwrap()),
        None => println!("Value not found."),
    }
    Ok(())
}

pub fn all_state() -> anyhow::Result<()> {
    let cached_config = get_cached_config()?;

    let storage = Storage::new(
        &cached_config.storage_dir,
        &cached_config.chain_name,
        &cached_config.state_hash,
        false,
    )?;

    println!("{}", storage);
    Ok(())
}

pub fn cached_config() -> anyhow::Result<()> {
    let cached_config = get_cached_config()?;
    println!(
        "Config values cached at {}:\n\n{}",
        CachedConfig::path().display(),
        toml::to_string_pretty(&cached_config).unwrap()
    );
    Ok(())
}

fn get_cached_config() -> anyhow::Result<CachedConfig> {
    CachedConfig::try_read()?.ok_or_else(|| {
        anyhow!(
            "expected a cached config to exist at {} from a previous 'exec' run",
            CachedConfig::path().display()
        )
    })
}
