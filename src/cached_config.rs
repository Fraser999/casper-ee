use anyhow::Context;
use std::path::PathBuf;
use std::{env, fs};

use casper_types::Digest;
use clap::crate_name;
use directories::ProjectDirs;
use log::debug;
use serde::{Deserialize, Serialize};

#[derive(Eq, PartialEq, Serialize, Deserialize, Debug)]
pub struct CachedConfig {
    pub storage_dir: PathBuf,
    pub chain_name: String,
    pub node_address: String,
    pub state_hash: Digest,
}

impl CachedConfig {
    /// Returns the `CachedConfig` if it exists or `None` if it doesn't.
    pub fn try_read() -> anyhow::Result<Option<Self>> {
        let config_path = Self::path();
        if !config_path.exists() {
            return Ok(None);
        }
        let encoded = fs::read_to_string(&config_path).with_context(|| {
            format!("failed to read cached config at {}", config_path.display())
        })?;
        toml::from_str(&encoded).with_context(|| {
            format!(
                "failed to decode cached config at {}",
                config_path.display()
            )
        })
    }

    pub fn write(&self) -> anyhow::Result<()> {
        let config_path = Self::path();
        let config_dir = config_path.parent().unwrap();
        fs::create_dir_all(config_dir).with_context(|| {
            format!(
                "failed to create dir for cached config at {}",
                config_dir.display()
            )
        })?;
        let encoded = toml::to_string_pretty(&self).context("failed to encode cached config")?;
        fs::write(&config_path, encoded).with_context(|| {
            format!("failed to write cached config at {}", config_path.display())
        })?;
        debug!("cached config at {}", config_path.display());
        Ok(())
    }

    pub fn path() -> PathBuf {
        if cfg!(test) {
            return env::temp_dir()
                .join(format!("test-{}", crate_name!()))
                .join("config.toml");
        }
        if let Some(project_dir) = ProjectDirs::from("", "Casper Labs", crate_name!()) {
            return project_dir.config_dir().join("config.toml");
        }
        env::temp_dir().join(crate_name!()).join("config.toml")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;

    #[test]
    fn toml_roundtrip() {
        let config = CachedConfig {
            storage_dir: Path::new("a/b/c.toml").to_path_buf(),
            chain_name: "casper-net-1".to_string(),
            node_address: "http://localhost:11101".to_string(),
            state_hash: Digest::hash(&[7, 8, 9]),
        };
        let encoded = toml::to_string_pretty(&config).unwrap();
        let decoded = toml::from_str(&encoded).unwrap();
        assert_eq!(config, decoded);
    }

    #[test]
    fn should_read_write() {
        let _ = fs::remove_file(CachedConfig::path());
        assert!(CachedConfig::try_read().unwrap().is_none());

        let config = CachedConfig {
            storage_dir: Path::new("a/b/c.toml").to_path_buf(),
            chain_name: "casper-net-1".to_string(),
            node_address: "http://localhost:11101".to_string(),
            state_hash: Digest::hash(&[7, 8, 9]),
        };
        config.write().unwrap();
        let read = CachedConfig::try_read().unwrap();
        assert_eq!(Some(config), read);
    }
}
