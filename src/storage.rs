use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};
use std::fs;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use anyhow::{bail, Context};
use casper_storage::global_state::state::CommitError;
use casper_types::execution::{ExecutionJournal, TransformKind};
use casper_types::{
    bytesrepr::{self, ToBytes},
    Digest, Key, StoredValue,
};
use log::{debug, error, trace};

/// A simple key, value store, with an in-mem map and an on-disk file for persisting the data.
#[derive(Clone)]
pub struct Storage {
    path: PathBuf,
    data: Rc<RefCell<HashMap<Key, StoredValue>>>,
}

impl Storage {
    /// Returns a new `Storage` backed by a file at "root_path/chain_name-short_hash" where
    /// short_hash is the first 7 chars of hex-encoded state_hash.
    ///
    /// If the file already exists, it is opened and parsed into the in-mem map.  If the file
    /// doesn't exist, it is created if `create` is `true`, otherwise an error is returned.
    ///
    /// The in-mem map is only written to disk when `persist()` is called or when the `Storage`
    /// instance is dropped.
    pub fn new(
        root_path: &Path,
        chain_name: &str,
        state_hash: &Digest,
        create: bool,
    ) -> anyhow::Result<Self> {
        let mut short_hash = format!("{:?}", state_hash);
        short_hash.truncate(7);
        let path = root_path.join(format!("{}-{}", chain_name, short_hash));

        if !path.is_file() {
            if create {
                return Ok(Storage {
                    path,
                    data: Rc::new(RefCell::new(HashMap::new())),
                });
            }
            bail!("no global state found at {}", path.display());
        }

        let serialized =
            fs::read(&path).with_context(|| format!("failed to read {}", path.display()))?;
        let data = bytesrepr::deserialize(serialized)
            .with_context(|| format!("failed to parse {}", path.display()))?;
        Ok(Storage {
            path,
            data: Rc::new(RefCell::new(data)),
        })
    }

    /// Insert the value to the in-mem map.
    pub fn insert(&self, key: Key, value: StoredValue) {
        self.data.borrow_mut().insert(key, value);
    }

    /// Get the value from the in-mem map.
    pub fn get(&self, key: &Key) -> Option<StoredValue> {
        self.data.borrow().get(key).cloned()
    }

    /// Put the effects to the in-mem map.
    pub fn commit(&self, effects: ExecutionJournal) -> Result<(), CommitError> {
        for effect in effects.transforms() {
            match effect.kind().clone() {
                TransformKind::Identity => {}
                TransformKind::Write(value) => {
                    trace!("put to storage: {}, {:?}", effect.key(), value);
                    self.insert(*effect.key(), value);
                }
                kind => {
                    let current_value = self
                        .data
                        .borrow()
                        .get(effect.key())
                        .ok_or_else(|| {
                            error!("failed to get {} from storage", effect.key());
                            CommitError::KeyNotFound(*effect.key())
                        })?
                        .clone();
                    let error_context =
                        format!("failed to apply {:?} to {:?}", kind, current_value);
                    let new_value = match kind.apply(current_value) {
                        Ok(value) => value,
                        Err(error) => {
                            error!("{error_context}: {}", error);
                            return Err(CommitError::TransformError(error));
                        }
                    };
                    trace!("put to storage: {}, {:?}", effect.key(), new_value);
                    self.insert(*effect.key(), new_value);
                }
            }
        }
        Ok(())
    }

    /// Write the in-mem map to disk.
    pub fn persist(&self) -> anyhow::Result<()> {
        let serialized = (*self.data.borrow())
            .to_bytes()
            .with_context(|| "failed to serialize storage")?;
        if let Some(dir) = self.path.parent() {
            fs::create_dir_all(dir)
                .with_context(|| format!("failed to create storage dir at {}", dir.display()))?;
        }
        fs::write(&self.path, serialized)
            .with_context(|| format!("failed to write to {}", self.path.display()))?;
        debug!("wrote global state to {}", self.path.display());
        Ok(())
    }
}

// impl Drop for Storage {
//     fn drop(&mut self) {
//         if Rc::strong_count(&self.data) == 1 {
//             let _ = self.persist();
//         }
//     }
// }

impl Display for Storage {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        writeln!(formatter, "Storage at {}", self.path.display())?;
        for (key, value) in self.data.borrow().iter() {
            writeln!(
                formatter,
                "  {}: {}",
                key,
                serde_json::to_string(value).unwrap()
            )?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use casper_types::{CLValue, HashAddr};

    #[test]
    fn should_persist_storage() {
        let temp_dir = tempfile::tempdir().unwrap();
        let root_path = temp_dir.path();
        let state_hash = Digest::hash(&[1]);

        let key = Key::Hash(HashAddr::from(Digest::hash(&[2])));
        let stored_value = StoredValue::CLValue(CLValue::from_t("stuff").unwrap());

        {
            let storage = Storage::new(root_path, "net", &state_hash).unwrap();
            storage
                .data
                .borrow_mut()
                .insert(key.clone(), stored_value.clone());
        }

        let storage = Storage::new(root_path, "net", &state_hash).unwrap();
        let retrieved_value = storage.data.borrow().get(&key).cloned().unwrap();
        assert_eq!(retrieved_value, stored_value);
    }
}
