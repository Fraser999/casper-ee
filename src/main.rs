mod cached_config;
mod cli;
mod state;
mod storage;
mod subcommands;

use tokio::runtime::Handle;

use crate::cli::Show;
use cached_config::CachedConfig;
use cli::{Cli, SnapshotId, UserProvidedOrDefault};
use state::State;
use storage::Storage;

// TODO
//   * get and use chainspec instead of default engine config and DeployConfig.  If either config fails to parse, probably worth not executing as different version EE on node.
//   * persist storage earlier and don't call on drop if not dirty
//   * build into client rather than spec exec

// Advantages
//   * Doesn't risk killing node via exploitable Deploy
//   * Lighter load on node
//   * Can run installer and then call stored contract
//   * Can use debugger in case of crash

async fn run_main() -> anyhow::Result<()> {
    env_logger::init();

    match Cli::build()? {
        Cli::Exec {
            storage_dir,
            chain_name,
            node_address,
            snapshot_id,
            transaction_path,
        } => subcommands::exec::run(
            storage_dir,
            chain_name,
            node_address,
            snapshot_id,
            transaction_path,
            Handle::current(),
        ),
        Cli::Show(Show::Key(key)) => subcommands::show::value(key),
        Cli::Show(Show::AllState) => subcommands::show::all_state(),
        Cli::Show(Show::CachedConfig) => subcommands::show::cached_config(),
        Cli::Clean => subcommands::clean::run(),
    }
}

fn main() -> anyhow::Result<()> {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(run_main())
}
