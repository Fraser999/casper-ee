# `casper-ee`

The `casper-ee` command line app allows Casper smart contract authors to test their contracts by running them locally
against a snapshot of global state from any accessible Casper network.  It also supports viewing values in global state
which have been fetched and possibly modified locally.

## Execution

The app requires a JSON-encoded Transaction (a.k.a Deploy) such as can be produced by the
[Rust casper-client](https://github.com/casper-ecosystem/casper-client-rs).

To execute a Transaction, use the `exec` command. This requires a few initial args to e.g. specify the network and
node to use for fetching global state. Such args are cached and need not be specified again unless you want to change
their values.

The results of execution are written to a local storage directory, meaning further runs can be executed on top of this.

### Examples

The default args are set up for targeting a local nctl-network:

```
casper-ee exec deploy-cep-78.json
```

To target Casper testnet at block height 100 and using a non-default storage directory:

```
casper-ee exec deploy-cep-78.json \
  --storage-dir /tmp/storage \
  --chain-name "casper-test" \
  --node-address http://65.108.0.22:7777 \
  --block-height 100
```

## View Global State

The local copy of global state can be viewed via the `show` command.

### Example

Show a value under a single key:

```
casper-ee show -k account-hash-eb6f3becd6464b64b5ed0d7a80caa268ed90c43fce2898a849a283b02e52a6b1
```
